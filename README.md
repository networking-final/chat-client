# About

The client part of a chat program for my networking final. It allows its users to connect to any server that matches the specification contained in this document.

Written in Neovim with `coc.nvim`.

## Build Instructions

1. Run [`go.sh`](build/go.sh) at the root of the project.
	* This will build the host for this application.
2. Run [`css.sh`](web/build/css.sh) at the "web/" directory.
3. Run [`js.sh`](web/build/js.sh) at the "web/" directory.
	* You may run [`js_debug.sh`](web/build/js_debug.sh) instead to generate non-minified JavaScript that is easier to debug.

## Dependendcies

### Client

* [Chromium](https://www.chromium.org/getting-involved/download-chromium) or [Firefox](https://www.mozilla.org/en-US/exp/firefox/new/)
	* Developed and tested on Chromium and Firefox Developer Edition.
	* Browsers _based_ on Chromium (like [Chrome](https://www.google.com/chrome/index.html)) or Firefox 57+ will work also.
		* Other browsers might work but are __unsupported__.

### Host

* [Dart](https://dart.dev/get-dart)
* [Go](golang.org)
	> __IMPORTANT:__ Make sure to have `$GOPATH` set up on the machine that will host the client.
* [Sass](https://github.com/sass/dart-sass) ([alt](https://pub.dev/packages/sass))

# Documentation

__Note:__ Unlike most EBNF (and even the BNF in the [Server Documentation](https://gitlab.com/networking-final/chat-server/-/blob/master/README.md)), the symbols `<` & `[`, as well as `>` and `]` are to be treated as the same.
	* Their use is dependent on whether or not HTML is being described. I have tried to keep things consistent, though, and removed ambiguous BNF syntax to do that.

## Data

### \<server_name> →

* Using definition of `<room_name>` from [Server Documentation](https://gitlab.com/networking-final/chat-server/-/blob/master/README.md).

```json
<room_name>
```

## Directories

All these directories are stored as keys in the browsers `localStorage`.
	* It is simply presented as a directory for the sake of explanation. Were this application to be re-implemented for the desktop, these objects would undoubtedly be directories.

### \<local_storage_dir> →

```json
"servers": {
	<server_dir>,
	({<server_dir>})
}
```

### \<room_dir> →

```json
"<room_id>": {
	"messages.json": <messages.json>,
	"room.json": <room.json>
}
```

### \<server_dir> →

```json
"<server_name>": {
	"rooms": {
		(<room_dir> {, <room_dir>})
	},
	"server.json": <server.json>,
	"session.ssl": "<session.ssl>"
}
```

## Files

### \<messages.json> →

* Using definition of `<messages>` from [server](https://gitlab.com/networking-final/chat-server).

```json
[
	(<message> {, <message>})
]
```

### \<room.json> →

```json
{
	"name": "<room_name>"
}
```

### \<server.json> →

```json
{
	"addr": <server_addr>,
	"username": "<client_name>"
}
```

### \<session.ssl> →

* Using definition of `[client_session]` from [server](https://gitlab.com/networking-final/chat-server).

```ssl
<client_session>
```

## HTML

### \<pane> →

```html
<div id="pane">
	[pane_sidebar]
	[pane_chat]
</div>
```

### \<pane_chat> →

```html
<div id="pane_chat">
	[pane_chat_header]
	[pane_chat_messages]
	[pane_chat_input]
</div>
```

### \<pane_chat_header> →

```html
<div id="pane_chat_header">
	<h1>[room_name]</h1>
</div>
```

### \<pane_chat_input> →

```html
<div id="pane_chat_input">
	<div id="pane_chat_input_text">
		<textarea cols="1" id="pane_chat_input_text_area" name="pane_chat_input_text_area" placeholder="Enter a message..."></textarea>
	</div>
	<button id="pane_chat_input_button">→</button>
</div>
```

### \<pane_chat_message> →

```html
<li class="pane_chat_message_sent">
	<header>You <time>[pane_chat_message_time]</time></header>
	[pane_chat_message_text]
</li>
```

#### |

```html
<li class="pane_chat_message_received">
	<header>[pane_chat_message_sender] <time>[pane_chat_message_time]</time></header>
	[pane_chat_message_text]
</li>
```

### \<pane_chat_message_list> →

```html
<ul id="pane_chat_messages">
	{[pane_chat_message]}
</ul>
```

### \<pane_chat_message_text> →

```html
<p>[pane_chat_message_text_string]</p>
```

#### |

```html
<p class="top">[pane_chat_message_text_string]</p>
{<p class="middle">[pane_chat_message_text_string]</p>}
<p class="bottom">[pane_chat_message_text_string]</p>
```

### \<pane_sidebar> →

```html
<div id="pane_sidebar">
	[pane_sidebar_setting_list]
	[pane_sidebar_room_list]
</div>
```

### \<pane_sidebar_setting_list> →

```html
<ul id="pane_sidebar_setting_list">
	{[pane_sidebar_setting]}
</ul>
```

### \<pane_sidebar_setting> →

```html
<li class="pane_sidebar_setting">
	<label class="pane_sidebar_setting_entry">[pane_sidebar_setting_name]</label>
	([pane_sidebar_setting_dropdown])
</li>
```

### \<pane_sidebar_setting_dropdown> →

```html
<div class="pane_sidebar_setting_dropdown">
	{[pane_sidebar_setting_dropdown_entry]}
</div>
```

### \<pane_sidebar_setting_dropdown_entry> →

```html
<div class="pane_sidebar_setting_dropdown_entry" id="[pane_sidebar_setting_dropdown_entry_id]">
	<input id="[input_id]" type="[input_type]" name="[input_name]" placeholder="[input_placeholder]"/>
	(<label for="[input_id]">[input_label]</label>)
</div>
```

### \<pane_sidebar_room_list> →

```html
<ul id="pane_sidebar_room_list">
	{[pane_sidebar_room]}
</ul>
```

### \<pane_sidebar_room> →

```html
<li class="pane_sidebar_room">
	<button>[room_name]</button>
	<data class="pane_sidebar_room_id" value="[room_id]"></data>
	<ul class="pane_sidebar_room_message_list">
		{[pane_chat_message]}
	</ul>
</li>
```

## JSON

### \<messages> →

`<message>` | `<messages>, <messages>`

### \<server_addr> →

* Using definition of `<client_addr>` from [Server Documentation](https://gitlab.com/networking-final/chat-server/-/blob/master/README.md).

```json
<client_addr>
```

# Usage

There are two ways to use this application. The second way is recommended, as using HTTP from a `file://` scheme is generally not recommended. However, it can still be done.

* Using `file://` scheme:
	1. Find "index.html".
		* On Linux, Execute `find . -type f -name "index.html"`.
		* On Mac/Windows, use your file brower's search function.
		* If all else fails, the file is in the "./web/web/" directory from the root of the repository.
	2. Navigate to "index.html" in your browser.
		* `:!firefox %` to open the file if you have it open in vim and you use Firefox.
		* `Ctrl-O` inside a web browser.
		* Double click on the file.
		* Start browser from commandline.
* Using `http://` scheme:
	1. Start hosting the `chat-client`. One of two ways:
		* Build the program following the build instructions (which you should do anyway), then execute `./bin/start-client -h` to get started.
			* The server will run faster if you build it before running it.
		* Skip building the project and execute `go run ./cmd/start-client/main.go -h` in the root of the project to get started.
		>
		> ## Note
		>
		> `-h` is the help flag. It will display the options and usage specific to the `start-client` command. It is recommended you view the help at least once before running it for the first time.
		>
	2. Connect to the address that appears on the screen after starting.
		* Connecting to this will allow you (and anyone else who can connect to the machine it is running on) to interact with the GUI and connect to a [chat server](https://gitlab.com/networking-final/chat-server).
