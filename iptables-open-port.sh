#!/usr/bin/env bash

# Note:
# These settings won't survive a reboot.
# To make them survive a reboot, run `iptables-save`, and then run `iptables-restore` on next boot.
iptables -I INPUT 1 -p tcp --dport "$1" -j ACCEPT && echo "Opened port $1."
