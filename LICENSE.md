# Chat-Client

__This work__ is licensed under the <u>Creative Commons Attribution-ShareAlike 4.0 International License</u>.

To view a copy of this license, visit [creativecommons.org](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to _Creative Commons, PO Box 1866, Mountain View, CA 94042, USA_.

# Dependendcies

* [dart-lang/markdown](https://github.com/dart-lang/markdown/blob/master/LICENSE)
* [dart-lang/pedantic](https://github.com/dart-lang/pedantic/blob/master/LICENSE)
* [dart-lang/sdk/pkg/meta](https://github.com/dart-lang/sdk/blob/master/pkg/meta/LICENSE)
