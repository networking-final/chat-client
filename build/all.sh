# Grab current directory.
cwd=$(pwd)

# Build the server.
build/go.sh

# Build the client.
cd ./web
build/css.sh
build/js.sh

# Return to previous directory.
cd "$cwd"
