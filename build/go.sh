#!/usr/bin/env bash

cmds=(start-client)
dir_dist="./bin"
dir_src="./cmd"

for c in ${cmds[*]} ; do
	go build -o "$dir_dist/$c" "$dir_src/$c/main.go"
done
