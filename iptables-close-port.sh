#!/usr/bin/env bash

# Note:
# Although `./iptables-open-port.sh` doesn't persist through rebooting,
#	this script can be used to undo the rule earlier.
iptables -D INPUT -p tcp --dport "$1" -j ACCEPT && echo "Closed port $1."
