package main
import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

func main() {
	flagBroadcast := flag.Bool("broadcast", false, "If true, open the server on more than just localhost.")
	optionPort := flag.Int("port", 0, "Specify the port the client should start on.")

	// Parse the args
	log.Println("Parsing flags.")
	flag.Parse()

	// Start the server with the options provided.
	defer log.Println("Shutting down client.")
	if err := startServer(*flagBroadcast, *optionPort); err != nil {
		panic(err)
	}
}

// Start the server on "http://[ip]:[port][urlSuffix]".
func startServer(broadcast bool, port int) error {
	// Define the URL on which we will perform operations.
	const urlSuffix string = "/chat-client/"

	// Get the current executible.
	_, exeFile, _, _ := runtime.Caller(0)
	pathToExe := strings.Split(exeFile,string(filepath.Separator))
	absPathToProjRoot := "/" + filepath.Join( pathToExe[:len(pathToExe)-3]... )

	// Get the current executing directory.
	currentDir, _ := os.Getwd()

	// Define the web dir.
	webAppDir, _ := filepath.Rel(currentDir, filepath.Join(absPathToProjRoot,"web","web"))
	log.Println("Serving:", webAppDir)

	// Create server multiplexer
	chatMux := http.NewServeMux()

	// Define something for the multiplexer to handle
	// In this, case, we want it to act when there is a request on [urlSuffix].
	// We want it to respond by serving the `index.html` at [webAppDir].
	chatMux.Handle(
		urlSuffix, http.StripPrefix(
			urlSuffix, http.FileServer(http.Dir(webAppDir)),
		),
	)

	// Determine what IP to listen on.
	var addr string; if broadcast {addr = ""} else {addr = "localhost"}

	// Begin listening.
	if listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", addr, port)); err != nil {

		// Don't continue if we have an error.
		panic(err)

	} else {

		// Remember to close the listener.
		defer func() {
			if err := listener.Close(); err != nil {
				log.Println("ERROR:", err.Error())
			}
		}()

		// Define the server options.
		server := http.Server{
			Handler:            chatMux,
			IdleTimeout:        30 * time.Second,
			ReadHeaderTimeout:  2 * time.Second,
			ReadTimeout:        time.Second,
			WriteTimeout:       time.Second,
		}

		// Remember to close the server.
		defer func() {
			if err := server.Close(); err != nil {
				log.Println("ERROR:", err.Error())
			}
		}()

		// Define instructions for using the server.
		var instructions strings.Builder; instructions.WriteString("\n\t\t\t")

		if broadcast {
			instructions.WriteString("The IP address of any interface can be used to access the client.")
		} else {
			instructions.WriteString("The client is accessable on localhost only.")
		}

		// Print the instructions.
		log.Println("Starting client on",
			fmt.Sprintf("'http://%s%s'.", listener.Addr(), urlSuffix), instructions.String(),
		)


		// Start the server.
		return server.Serve(listener)
	}
}
