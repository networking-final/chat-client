import 'package:networkingfinal_client/chat_client.dart' show PaneSidebarSettingConnect, PaneSidebarSettingTheme;

void main()
{
	// Listen for the connect button.
	PaneSidebarSettingConnect.listenConnectClick();
	// Listen for the theme settings.
	PaneSidebarSettingTheme.listenSelector();
}
