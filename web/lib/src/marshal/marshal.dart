import 'dart:convert';

/// A class that can be marshalled to and from JSON.
abstract class Marshal<T>
{
	static final JsonCodec _codec = new JsonCodec();

	/// Convert a [json] [String] into an [Object] of type [T].
	static T convert<T>(String json)
	{
		return _codec.decode(json) as T;
	}

	/// Convert a [Map<String, dynamic>] back into a JSON [String].
	static String revert(Map<String, dynamic> json)
	{
		return _codec.encode(json);
	}

	/// Convert this instance of [Marshal] back to a JSON-[convertBack()]-able [Object].
	T toJson();
}
