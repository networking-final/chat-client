import 'package:networkingfinal_client/chat_client.dart';

/// A <server_dir>.
class ServerDir implements Marshal<Map<String, dynamic>>
{
	/*
	 * MEMBERS
	 */
	
	/// The JSON key for the [_roomIdMap] field.
	static const String JSON_SERVER_DIR_ROOMS     = 'rooms';
	/// The JSON key for the [_serverFile] field.
	static const String JSON_SERVER_DIR_ROOM_FILE = 'server.json';
	/// The JSON key for accessing [ServerDir]s in `localStorage`.
	static const String JSON_SERVER_SESSION_FILE  = 'session.ssl';

	final Map<String, RoomDir> _roomIdMap;
	final ServerDotJson        _serverFile;
	      String               _session = '';

	/*
	 * CONSTRUCTORS
	 */
	
	/// Create a [ServerDir] from scratch.
	ServerDir(this._roomIdMap, this._serverFile, [this._session]);

	/// Parse some [json] and create an instance of the [RoomDotJson] class.
	ServerDir.fromJson(Map<String, dynamic> json): this(
		// `json.keys.first` holds the server name.
		(json[JSON_SERVER_DIR_ROOMS] as Map<String, dynamic>).map<String, RoomDir>(
			(String key, dynamic value) => new MapEntry<String, RoomDir>(
				key, new RoomDir.fromJson(value as Map<String, dynamic>)
			)
		),
		new ServerDotJson.fromJson(
			json[JSON_SERVER_DIR_ROOM_FILE] as Map<String, dynamic>
		),
		json[JSON_SERVER_SESSION_FILE] as String,
	);

	/*
	 * GETTERS
	 */
	
	/// The map of <room_id>s to <room_dir>s.
	Map<String, RoomDir> get roomIdMap  => this._roomIdMap;
	/// The <server.json>.
	ServerDotJson        get serverFile => this._serverFile;
	/// The <session.ssl>.
	String               get session    => this._session;

	/*
	 * SETTERS
	 */
	
	/// Set [session] to [newSession].
	set session (String newSession) => this._session = newSession;

	/// Get a [RoomDir] from [roomIdMap] using [roomId] as a key.
	RoomDir operator [] (String roomId) => this.roomIdMap[roomId];

	/// Assign a [RoomDir] [value] to [roomIdMap] using [roomId] as a key.
	void operator []= (String roomId, RoomDir value)
		=> this.roomIdMap[roomId] = value;

	/*
	 * METHODS
	 */

	@override
	String toString()
	{
		return 'SERVER_DIR:'
				'\n\tRooms: $roomIdMap'
				'\n\tServer File: $serverFile'
				'\n\tSession File: $session';
	}
	
	@override
	Map<String, dynamic> toJson()
	{
		return <String, dynamic>{
			JSON_SERVER_DIR_ROOMS: this.roomIdMap.map<String, dynamic>(
				(String key, RoomDir value) => new MapEntry<String, dynamic>(
					key, value.toJson()
				)
			),
			JSON_SERVER_DIR_ROOM_FILE : this.serverFile.toJson(),
			JSON_SERVER_SESSION_FILE  : this.session
		};
	}
}
