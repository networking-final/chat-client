import 'dart:html';

import 'package:meta/meta.dart';

import 'package:networkingfinal_client/chat_client.dart';

/// A <room_dir>.
class RoomDir implements Marshal<Map<String, dynamic>>
{
	/*
	 * MEMBERS
	 */

	/// The JSON key for the [_messagesDotJson] field.
	static const String JSON_ROOM_DIR_MESSAGES_FILE = 'messages.json';
	/// The JSON key for the [_roomDotJson] field.
	static const String JSON_ROOM_DIR_ROOM_FILE     = 'room.json';

	final MessagesDotJson _messagesDotJson;
	final RoomDotJson     _roomDotJson;

	/*
	 * CONSTRUCTORS
	 */

	/// Create a [RoomDir] from scratch.
	RoomDir(this._messagesDotJson, this._roomDotJson);

	/// Parse some [json] and create an instance of the [RoomDotJson] class.
	RoomDir.fromJson(Map<String, dynamic> json) : this(
		new MessagesDotJson.fromJson(
			json[JSON_ROOM_DIR_MESSAGES_FILE] as List<dynamic>
		),
		new RoomDotJson.fromJson(
			json[JSON_ROOM_DIR_ROOM_FILE] as Map<String, dynamic>
		)
	);

	/*
	 * GETTERS
	 */

	/// The "id" field.
	MessagesDotJson get messagesDotJson => this._messagesDotJson;
	/// The "name" field.
	RoomDotJson     get roomDotJson     => this._roomDotJson;

	/*
	 * METHODS
	 */

	@override
	String toString()
	{
		return 'ROOM_DIR:'
				'\n\tMESSAGES.JSON: $messagesDotJson'
				'\n\tROOM.JSON: $roomDotJson';
	}

	@override
	Map<String, dynamic> toJson()
	{
		return <String, dynamic>{
			JSON_ROOM_DIR_MESSAGES_FILE : this.messagesDotJson.toJson(),
			JSON_ROOM_DIR_ROOM_FILE     : this.roomDotJson.toJson(),
		};
	}

	/// Update the DOM with the content stored.
	/// Requires this [RoomDir]'s [roomId] (which is not stored in the class), and the [username] of the client in order to determine how to turn the JSON `localStorage` into HTML.
	void updatePaneSidebar({@required String roomId, @required String username})
	{
		final Log log = new Log('updatePaneSidebar()')

		// Create element to represent this object. See documentation for more details.
		..write('Creating ".pane_sidebar_room" <li>.');
		final LIElement roomDirAsHtml = new LIElement()..classes.add('pane_sidebar_room');

		// Add the button element
		roomDirAsHtml.children.add(new ButtonElement()..innerText = this.roomDotJson.name);
		log.write('Created <button>.');

		// Add the data element
		roomDirAsHtml.children.add(
			Element.html('<data class="pane_sidebar_room_id" value="$roomId"></data>')
		);
		log.write('Created <data>.');

		// Create a holder for the messages.
		final UListElement messagesAsHtml = new UListElement()..classes.add('pane_sidebar_room_message_list');
		log..write('Created <ul> to hold [Message]s.')

		// Add all the messages
		..write('Looping over messages and filling the <ul>.');
		for (final Message msg in this.messagesDotJson.contents)
		{
			try
			{
				// Make sure there is a pre-existing message. If the children are not empty, there must be a message.
				// NOTE: this should be enough to make sure that there is at least one message, but I'm not 100% positive.
				log.write('Testing if there are any messages loaded.');
				if (messagesAsHtml.children.isNotEmpty)
				{
					// Try to reduce the next message instead of creating a DOM element.
					log.write('Messages found. Trying to reduce the last two messages.', depth:1);
					messagesAsHtml.children.last = Message.reduceHtml(messagesAsHtml.children.last as LIElement, msg);

					// If there has not been an error, `continue` (because by letting the for loop go on, a duplicate message will get created.)
					log.write('Message html reduced. Continuing to load messages...', depth: 2);
					continue;
				}
				else
				{ log.write('No previously loaded messages.',depth:1); }
			}
			// The messages were not successfully reduced.
			on ArgumentError
			{
				log.write('Previous messages found, but they were not suitable for reducing.', depth: 2);
			}

			// Add the message to the list of messages.
			log.write('Creating new HTML element from the [msg].', depth: 1);
			messagesAsHtml.children.add( msg.toHtml(username) );
		}

		// Add the messages HTML to the room dir HTML.
		log.write('Adding the <ul> to the [RoomDir] <li>.');
		roomDirAsHtml.children.add(messagesAsHtml);

		// Add the room dir HTML to the DOM.
		log.write('Adding the [RoomDir] <li> to the sidebar.');
		Query.paneSidebarRoomList().children.add(roomDirAsHtml);
	}
}
