import 'dart:html';
import 'package:networkingfinal_client/chat_client.dart';

/// A <local_storage_dir>.
abstract class LocalStorageDir
{
	/*
	 * MEMBERS
	 */

	/// The JSON key for the [_serverNameMap] field.
	static const String JSON_LOCAL_STORAGE_DIR_SERVERS = 'servers';

	/// The "servers" field of `localStorage`.
	static final Map<String, ServerDir> _serverNameMap = () {
		try
		{
			return (
				Marshal.convert<Map<String, dynamic>>(
					LocalStorageDir._readStorage() // May throw [StateError]
				)[JSON_LOCAL_STORAGE_DIR_SERVERS] as Map<String, dynamic>
			).map<String, ServerDir>(
				(String key, dynamic value) => new MapEntry<String, ServerDir>( key, new ServerDir.fromJson(value as Map<String, dynamic>) )
			);
		}
		on StateError
		{
			return new Map<String, ServerDir>();
		}
	}();

	/*
	 * GETTERS
	 */

	/// Return whether or not the internal [_serverNameMap] contains the specified [key].
	static bool containsKey(String key) => LocalStorageDir._serverNameMap.containsKey(key);

	/// Get a [ServerDir] from [_serverNameMap] using [serverName] as a key.
	static ServerDir get(String serverName) => LocalStorageDir._serverNameMap[serverName];

	/// Get all of the keys in [_serverNameMap].
	static Iterable<String> get keys => LocalStorageDir._serverNameMap.keys;

	/*
	 * SETTERS
	 */

	/// Assign a [ServerDir] [value] to [_serverNameMap] using [serverName] as a key.
	static void put(String serverName, ServerDir value) => LocalStorageDir._serverNameMap[serverName] = value;

	/*
	 * METHODS
	 */

	/// Read the local storage for this object.
	/// Throws a [StateError] if `localStorage` has no key for [JSON_LOCAL_STORAGE_DIR_SERVERS].
	static String _readStorage()
	{
		if (window.localStorage.containsKey(JSON_LOCAL_STORAGE_DIR_SERVERS))
		{
			return window.localStorage[JSON_LOCAL_STORAGE_DIR_SERVERS];
		}
		else
		{
			throw new StateError('`localStorage` is empty.');
		}
	}

	@override
	String toString()
	{
		return 'LOCAL_STORAGE_DIR:'
				'\n\tServer Dirs: $_serverNameMap';
	}

	/// Convert the [LocalStorageDir] to JSON.
	static Map<String, dynamic> toJson()
	{
		return <String, dynamic>{
			JSON_LOCAL_STORAGE_DIR_SERVERS: LocalStorageDir._serverNameMap.map<String, dynamic>(
				(String key, ServerDir value) => new MapEntry<String, dynamic>(
					key, value.toJson()
				)
			),
		};
	}

	/// Write to the `localStorage` for this object.
	static void shelve() => window.localStorage[JSON_LOCAL_STORAGE_DIR_SERVERS] = Marshal.revert(LocalStorageDir.toJson());
}
