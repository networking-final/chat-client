import 'dart:html';

import 'package:markdown/markdown.dart' show markdownToHtml;

import 'package:networkingfinal_client/src/marshal/marshal.dart';
import 'package:networkingfinal_client/chat_client.dart' show Log;

/// A <message>.
class Message implements Marshal<Map<String, dynamic>>
{
	/*
	 * MEMBERS
	 */

	/// The JSON key for the [_body] field.
	static const String JSON_MESSAGE_BODY   = 'body';
	/// The JSON key for the [_client] field.
	static const String JSON_MESSAGE_CLIENT = 'client';
	/// The JSON key for the [_time] field.
	static const String JSON_MESSAGE_TIME   = 'time';

	String _body;
	String _client;
	String _time;

	/*
	 * CONSTRUCTORS
	 */

	/// Create a [Message] from scratch.
	Message(this._client, this._body, this._time);

	/// Parse some [json] and create an instance of the [Message] class.
	Message.fromJson( Map<String, dynamic> json ) : this(
		json[JSON_MESSAGE_CLIENT] as String,
		json[JSON_MESSAGE_BODY  ] as String,
		json[JSON_MESSAGE_TIME  ] as String
	);

	/*
	 * GETTERS
	 */

	/// The "body" field.
	String get body   => this._body;
	/// The "client" field.
	String get client => this._client;
	/// The "time" field.
	String get time   => this._time;

	/*
	 * SETTERS
	 */

	set body   (String newContent) => this._body   = newContent;
	set client (String newClient)  => this._client = newClient;
	set time   (String newTime)    => this._time   = newTime;

	/*
	 * METHODS
	 */

	static Element  _getHeader(LIElement e)           => e.children.firstWhere( (Element e) => e.tagName.toLowerCase() == 'header' );
	static String   _getHeaderClientName(LIElement e) => _getHeader(e).innerText.split(' ').first;
	static DateTime _getHeaderTime(LIElement e)       => (_getHeader(e).innerText.split(' ')..removeAt(0)).join(' ').asDateTime();

	/// Check whether or not to append [newMsg]'s body to [prevMsgHtml].
	/// Throws [ArgumentError] when the messages do not fit combination criteria.
	///
	/// __Criteria:__
	/// 1. Check if client names are the same.
	/// 2. Check if messages have been sent 5 minutes or less apart.
	static LIElement reduceHtml(LIElement prevMsgHtml, Message newMsg)
	{
		final Log log = new Log('reduceHtml()')..write(
			'Analyzing [prevMsgHtml] and [newMsg] to determine if they can be combined.'
		)..write('Client Name:', depth: 1)..write(
			'[prevMsgHtml]: ${Message._getHeaderClientName(prevMsgHtml)}',
			depth: 2
		)..write(
			'[newMsg]: ${newMsg.client}',
			depth: 2
		)..write('Timestamp:', depth: 1)..write(
			'[prevMsgHtml]: ${Message._getHeaderTime(prevMsgHtml)}',
			depth: 2
		)..write(
			'[newMsg]: ${newMsg.time.messageTimeFmt().asDateTime()}',
			depth: 2
		);

		if
		(
			Message._getHeaderClientName(prevMsgHtml) == newMsg.client
			&& Message._getHeaderTime(prevMsgHtml).difference( newMsg.time.messageTimeFmt().asDateTime() ).inMinutes <= 5
		)
		{
			log.write('Attempting to combine HTML');

			final int preExistingMessages = prevMsgHtml.children.whereType<ParagraphElement>().length;

			// Set the first message to class `top`.
			// We only want to do this the first time combining a message. It would be a waste to do this every single time.
			if (preExistingMessages < 2)
			{
				prevMsgHtml.children.last.classes..clear()..add('top');
			}

			// Set the previous-last message to class of `middle`.
			// If there was only one previous message, there would be no "last" message to adjust to `middle`.
			// Unlike the previous `if`, this _must_ be done every time.
			if (preExistingMessages > 1)
			{
				prevMsgHtml.children.last.classes..clear()..add('middle');
			}

			// Set new message to be `bottom` class.
			prevMsgHtml.children.add(
				new ParagraphElement()
					..classes.add('bottom')
					..innerHtml = newMsg.toHtml(newMsg.client).children.firstWhere((Element e ) => e is ParagraphElement).innerHtml
			);

			// Return the message now that it has been combined.
			return prevMsgHtml;
		}
		else
		{
			throw new ArgumentError('The two messages could not be combined.');
		}
	}

	/// Convert this [Message] into its HTML [LIElement] form as per the documentation.
	LIElement toHtml(String username)
	{
		new Log('Message.toHtml()').write('Converting to HTML.');

		// Add the message to the list of messages.
		return new LIElement()
			// Determine whether or not the message was sent or received based on the username.
			..classes.add( (this.client == username) ? 'pane_chat_message_sent' : 'pane_chat_message_received' )
			// Format the message into HTML.
			..setInnerHtml('<header>${this.client} <time>${this.time.messageTimeFmt()}</time></header>\n${markdownToHtml(this.body)}');
	}

	@override
	String toString()
	{
		return 'MESSAGE:'
				'\n\tClient: $client'
				'\n\tContent: $body'
				'\n\tTime: $time';
	}

	@override
	Map<String, String> toJson()
	{
		return <String, String>{
			JSON_MESSAGE_BODY   : this.body,
			JSON_MESSAGE_CLIENT : this.client,
			JSON_MESSAGE_TIME   : this.time,
		};
	}
}

/// Extra methods for formatting [String]s into human readable text.
/// Helpful for use with the [Message] class.
extension DateTimeFmt on String
{
	/// Format's a string according to the `<message>` "time" field.
	/// Turns "`<year>`-`<month>`-`<day>`_`<hour>`-`<minute>`-`<second>`" into "`<year>`/`<month>`/`<day>` `<hour>`:`<minute>`:`<second>`"
	String messageTimeFmt()
	{
		const int DATE = 0;
		const int TIME = 1;

		final List<String> splitDateTime = this.split('_');

		final List<String> splitTime = new List<String>();

		for (final String s in splitDateTime[TIME].split('-'))
		{
			splitTime.add( s.padLeft(2,'0') );
		}

		return '${splitDateTime[DATE].replaceAll('-', '/')} ${splitTime.join(':')}';
	}

	/// Turns a String in [messageTimeFmt()] into a [DateTime].
	DateTime asDateTime()
	{
		const int DATE   = 0;
		const int DAY    = 2;
		const int HOUR   = 0;
		const int MINUTE = 1;
		const int MONTH  = 1;
		const int SECOND = 2;
		const int TIME   = 1;
		const int YEAR   = 0;

		final List<String> splitDateTime = this.split(' ');

		final List<List<int>> formatted = (
			<List<String>>[splitDateTime[DATE].split('/'), splitDateTime[TIME].split(':')]
		).map<List<int>>(
			(List<String> l) => l.map<int>(int.parse).toList()
		).toList();

		return new DateTime(
			formatted[DATE][YEAR],
			formatted[DATE][MONTH],
			formatted[DATE][DAY],
			formatted[TIME][HOUR],
			formatted[TIME][MINUTE],
			formatted[TIME][SECOND],
		);
	}
}
