import 'package:networkingfinal_client/chat_client.dart';

/// A <client>.
class Client implements Marshal<Map<String, String>>
{
	/*
	 * MEMBERS
	 */

	/// The JSON key for the [_name] field.
	static const String JSON_CLIENT_NAME    = 'name';
	/// The JSON key for the [_session] field.
	static const String JSON_CLIENT_SESSION = 'session';

	String _name;
	String _session;

	/*
	 * CONSTRUCTORS
	 */

	/// Create a [Client] from scratch.
	Client(this._name, this._session);

	/// Parse some [json] and create an instance of the [Client] class.
	Client.fromJson(Map<String, dynamic> json) : this(
		json[JSON_CLIENT_NAME]    as String,
		json[JSON_CLIENT_SESSION] as String,
	);

	/*
	 * GETTERS
	 */

	/// The "name" field.
	String get name    => this._name;

	/// The "session" field.
	String get session => this._session;

	/*
	 * SETTERS
	 */

	set name    (String newName)    => this._name = newName;
	set session (String newSession) => this._session = newSession;

	@override
	String toString()
	{
		return 'CLIENT:'
				'\n\tName: $name'
				'\n\tSession: $session';
	}

	@override
	Map<String, String> toJson()
	{
		return <String, String>{
			JSON_CLIENT_NAME    : this.name,
			JSON_CLIENT_SESSION : this.session,
		};
	}
}
