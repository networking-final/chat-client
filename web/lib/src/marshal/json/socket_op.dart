import 'package:networkingfinal_client/chat_client.dart';

/// A <socket_op>.
class SocketOp implements Marshal<Map<String, String>>
{
	/*
	 * MEMBERS
	 */
	
	/// The JSON key for the [_contentType] field.
	static const String JSON_SOCKET_OP_CONTENT_TYPE  = 'content_type';
	/// The JSON key for the [_opType] field.
	static const String JSON_SOCKET_OP_TYPE          = 'op_type';

	/// The <op_type> for HTTP status code 400.
	static const String OP_TYPE_ERR_400 = '400';
	/// The <op_type> for HTTP status code 403.
	static const String OP_TYPE_ERR_403 = '403';
	/// The <op_type> for a GET request.
	static const String OP_TYPE_GET     = 'get';
	/// The <op_type> for an acknowledgement.
	static const String OP_TYPE_OK      = 'ok';
	/// The <op_type> for a POST event.
	static const String OP_TYPE_POST    = 'post';

	/// <content_type> for <client>.
	static const String TYPE_CLIENT = 'client';
	/// <content_type> for <client_session>.
	static const String TYPE_CLIENT_SESSION = 'client_session';
	/// <content_type> for <FETCH>.
	static const String TYPE_FETCH = 'fetch';
	/// <content_type> for <message>.
	static const String TYPE_MSG = 'message';
	/// <content_type> for <message_dequeued>.
	static const String TYPE_QUEUE = 'queue';
	/// <content_type> for <rooms>.
	static const String TYPE_ROOMS = 'rooms';

	final String _contentType;
	final String _opType;

	/*
	 * CONSTRUCTORS
	 */
	
	/// Create a [SocketOp] from scratch.
	/// It is named "_" because it should be private.
	SocketOp._(this._contentType, this._opType);

	/// Create a new socket operation formatted like a 'get' <socket_op>.
	SocketOp.asGet(String contentType) : this._(contentType, 'get');

	/// Create a new socket operation formatted like an 'ok' <socket_op>.
	SocketOp.asOk(String contentType) : this._(contentType, 'ok');

	/// Create a new socket operation formatted like a 'post' <socket_op>.
	SocketOp.asPost(String contentType) : this._(contentType, 'post');

	/// Parse some [json] and create an instance of the [SocketOp] class.
	SocketOp.fromJson( Map<String, dynamic> json ) : this._(
		json[JSON_SOCKET_OP_CONTENT_TYPE] as String,
		json[JSON_SOCKET_OP_TYPE]         as String
	);

	/*
	 * GETTERS
	 */
	
	/// The "content_type" field.
	String get contentType => this._contentType;
	/// The "op_type" field.
	String get opType      => this._opType;

	/*
	 * METHODS
	 */

	@override
	String toString()
	{
		return 'SOCKET_OP:'
				'\n\tContent Type: $contentType'
				'\n\tOp Type: $opType';
	}
	
	@override
	Map<String, String> toJson()
	{
		return <String, String>{
			JSON_SOCKET_OP_CONTENT_TYPE : this.contentType,
			JSON_SOCKET_OP_TYPE         : this.opType,
		};
	}
}
