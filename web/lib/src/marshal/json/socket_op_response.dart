import 'package:networkingfinal_client/chat_client.dart';

/// A <socket_op> response.
class SocketOpResponse implements Marshal<Map<String, dynamic>>
{
	/*
	 * MEMBERS
	 */

	/// The JSON key for the [_content] field.
	static const String JSON_SOCKET_OP_RESPONSE_CONTENT = 'content';
	/// The JSON key for the [_operation] field.
	static const String JSON_SOCKET_OP_RESPONSE_OP      = 'operation';

	      dynamic  _content;
	      dynamic  _contentJson;
	final SocketOp _operation;

	/*
	 * CONSTRUCTORS
	 */

	/// Create a [SocketOpResponse] from scratch.
	SocketOpResponse(this._content, this._operation);

	/// Parse some [json] and create an instance of the [SocketOpResponse] class.
	SocketOpResponse.fromJson(Map<String, dynamic> json) :
		this._contentJson = json[JSON_SOCKET_OP_RESPONSE_CONTENT],
		this._operation = new SocketOp.fromJson(
			json[JSON_SOCKET_OP_RESPONSE_OP] as Map<String, dynamic>
		);

	/*
	 * GETTERS
	 */

	/// The "content" field.
	/// __Does not get created automatically__ by [SocketOpResponse.fromJson()].
	dynamic get content => this._content;
	/// The "content" field's in-memory JSON object.
	/// Can be used to create [content] later.
	dynamic  get contentJson => this._contentJson;
	/// The "operation" field.
	SocketOp get operation => this._operation;

	/*
	 * SETTERS
	 */

	/// Set the "content" field's value.
	/// __Does not get created automatically__ by [SocketOpResponse.fromJson()].
	set content (dynamic newContent) => this._content = newContent;

	/*
	 * METHODS
	 */

	/// Analyzes some [json] and determines if its type.
	/// * The acceptable types of [json] are [SocketOp] and [SocketOpResponse].
	/// * Will throw an [ArgumentError] if the [json] does not match a valid pattern.
	static Marshal<Map<String, dynamic>> analyze(Map<String, dynamic> json)
	{
		// It is a [SocketOp]
		if (json.containsKey(SocketOp.JSON_SOCKET_OP_TYPE))
		{
			return new SocketOp.fromJson(json);
		}
		// It is a [SocketOpResponse]
		else if (json.containsKey(JSON_SOCKET_OP_RESPONSE_OP))
		{
			return new SocketOpResponse.fromJson(json);
		}
		else
		{
			throw new ArgumentError('JSON does not match any compatible type.');
		}
	}


	@override
	String toString()
	{
		return 'SOCKET_OP_RESPONSE:'
				'\n\tContent: $content'
				'\n\tOperation: $operation';
	}
/// Convert some [SocketOpResponse] instance back to JSON.
	@override
	Map<String, dynamic> toJson()
	{
		return <String, dynamic>{
			JSON_SOCKET_OP_RESPONSE_CONTENT : this.content.toJson(),
			JSON_SOCKET_OP_RESPONSE_OP      : this.operation.toJson(),
		};
	}
}
