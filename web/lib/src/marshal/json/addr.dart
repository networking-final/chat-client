import 'package:networkingfinal_client/src/marshal/marshal.dart';

/// A <client_addr> __OR__ <server_addr>.
class ServerAddr implements Marshal<Map<String, dynamic>>
{
	/*
	 * MEMBERS
	 */
	
	/// The JSON key for the [_ip] field.
	static const String JSON_ADDR_IP  = 'ip';

	/// The JSON key for the [_port] field.
	static const String JSON_ADDR_PORT = 'port';

	String _ip;
	int    _port;

	/*
	 * CONSTRUCTORS
	 */
	
	/// Create a [ServerAddr] from scratch.
	ServerAddr(this._ip, this._port);

	/// Parse some [json] and create an instance of the [ServerAddr] class.
	ServerAddr.fromJson(Map<String, dynamic> json) : this(
		json[JSON_ADDR_IP  ] as String,
		json[JSON_ADDR_PORT] as int
	);

	/*
	 * GETTERS
	 */
	
	/// The "ip" field.
	String get ip   => this._ip;
	/// The "port" field.
	int    get port => this._port;

	/*
	 * SETTERS
	 */
	
	set ip   (String newIp) => this._ip   = newIp;
	set port (int newPort)  => this._port = newPort;
	// not sponsored ^^^

	/*
	 * METHODS
	 */

	@override
	String toString()
	{
		return 'ADDR:'
				'\n\tIP: $ip'
				'\n\tPORT: $port';
	}
	
	@override
	Map<String, dynamic> toJson()
	{
		return <String, dynamic>{
			JSON_ADDR_IP   : this.ip,
			JSON_ADDR_PORT : this.port,
		};
	}
}
