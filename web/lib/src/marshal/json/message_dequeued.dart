import 'package:networkingfinal_client/chat_client.dart';

/// A <message_dequeued>.
class MessageDequeued implements Marshal<Map<String, dynamic>>
{
	/*
	 * MEMBERS
	 */
	
	/// The JSON key for the [_message] field.
	static const String JSON_MESSAGE_DEQUEUED_MESSAGE = 'message';
	/// The JSON key for the [_room] field.
	static const String JSON_MESSAGE_DEQUEUED_ROOM_ID = 'room';

	final Message _message;
	      String  _room;

	/*
	 * CONSTRUCTORS
	 */
	
	/// Create a [MessageDequeued] from scratch.
	MessageDequeued(this._message, this._room);

	/// Parse some [json] and create an instance of the [MessageDequeued] class.
	MessageDequeued.fromJson(Map<String, dynamic> json) : this(
		new Message.fromJson(
			json[JSON_MESSAGE_DEQUEUED_MESSAGE] as Map<String, dynamic>
		),
		json[JSON_MESSAGE_DEQUEUED_ROOM_ID] as String
	);

	/*
	 * GETTERS
	 */
	
	/// The "message" field.
	Message get message => this._message;
	/// The "room" field.
	String  get room    => this._room;

	/*
	 * SETTERS
	 */
	
	/// Set the "room" field value.
	set room (String newRoom) => this._room = newRoom;

	/*
	 * METHODS
	 */

	@override
	String toString()
	{
		return 'MESSAGE_DEQUEUED:'
				'\n\tMessage: $message'
				'\n\tRoom: $room';
	}
	
	@override
	Map<String, dynamic> toJson()
	{
		return <String, dynamic>{
			JSON_MESSAGE_DEQUEUED_MESSAGE: this.message.toJson(),
			JSON_MESSAGE_DEQUEUED_ROOM_ID: this.room,
		};
	}
}
