import 'package:networkingfinal_client/chat_client.dart';

/// A <room_info>.
class RoomInfo implements Marshal<Map<String, String>>
{
	/*
	 * MEMBERS
	 */
	
	/// The JSON key for the [_id] field.
	static const String JSON_ROOM_ID   = 'id';
	/// The JSON key for the [_name] field.
	static const String JSON_ROOM_NAME = 'name';

	final String _id;
	final String _name;

	/*
	 * CONSTRUCTORS
	 */

	/// A private named constructor that will assign values to all the instance members of [RoomInfo].
	RoomInfo._(this._id, this._name);
	
	/// Create a new [RoomInfo] from some [json].
	RoomInfo.fromJson(Map<String, dynamic> json): this._(
		json[JSON_ROOM_ID]   as String,
		json[JSON_ROOM_NAME] as String,
	);

	/*
	 * GETTERS
	 */
	
	/// The "id" field.
	String get id => this._id;
	/// The "name" field.
	String get name => this._name;

	/*
	 * METHODS
	 */
	
	@override
	String toString()
	{
		return 'ROOM_INFO:'
				'\n\tId: $id'
				'\n\tName: $name';
	}
	
	@override
	Map<String, String> toJson()
	{
		return <String, String>{
			JSON_ROOM_ID   : this.id,
			JSON_ROOM_NAME : this.name,
		};
	}
}
