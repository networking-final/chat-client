import 'package:networkingfinal_client/src/marshal/marshal.dart';

/// A <room.json>. Also functions as a <room_info>.
/// * <room_infos> is a `List<RoomDotJson>`.
class RoomDotJson implements Marshal<Map<String, String>>
{
	/*
	 * MEMBERS
	 */
	
	/// The JSON key for the [_name] field.
	static const String JSON_ROOM_DOT_JSON_NAME = 'name';

	String _name;

	/*
	 * CONSTRUCTORS
	 */
	
	/// Create a [RoomDotJson] from scratch.
	RoomDotJson(this._name);

	/// Parse some [json] and create an instance of the [RoomDotJson] class.
	RoomDotJson.fromJson( Map<String, dynamic> json ) : this(
		json[JSON_ROOM_DOT_JSON_NAME]    as String
	);

	/*
	 * GETTERS
	 */
	
	/// The "name" field.
	String get name => this._name;

	/*
	 * SETTERS
	 */
	
	/// Set the "name" field value.
	set name (String newName) => this._name = newName;

	/*
	 * METHODS
	 */
	
	@override
	String toString()
	{
		return 'ROOM.JSON:'
				'\n\tName: $name';
	}

	@override
	Map<String, String> toJson()
	{
		return <String, String>{
			JSON_ROOM_DOT_JSON_NAME    : this.name,
		};
	}
}
