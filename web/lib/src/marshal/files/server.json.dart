import 'package:networkingfinal_client/src/marshal/json/addr.dart';
import 'package:networkingfinal_client/src/marshal/marshal.dart';

/// A <server.json>.
class ServerDotJson implements Marshal<Map<String, dynamic>>
{
	/*
	 * MEMBERS
	 */
	
	/// The JSON key for the [_addr] field.
	static const String JSON_SERVER_DOT_JSON_ADDR     = 'addr';
	/// The JSON key for the [_username] field.
	static const String JSON_SERVER_DOT_JSON_USERNAME = 'username';

	final ServerAddr   _addr;
	      String _username;

	/*
	 * CONSTRUCTORS
	 */
	
	/// Create a [ServerDotJson] from scratch.
	ServerDotJson(this._addr, this._username);

	/// Parse some [json] and create an instance of the [ServerDotJson] class.
	ServerDotJson.fromJson(Map<String, dynamic> json) : this(
		new ServerAddr.fromJson(
			json[JSON_SERVER_DOT_JSON_ADDR] as Map<String, dynamic>
		),
		json[JSON_SERVER_DOT_JSON_USERNAME] as String
	);

	/*
	 * GETTERS
	 */
	
	/// The "addr" field.
	ServerAddr   get addr => this._addr;
	/// The "username" field.
	String get username => this._username;

	/*
	 * SETTERS
	 */
	
	/// Set the "username" field value.
	set username (String newUsername) => this._username = newUsername;

	/*
	 * METHODS
	 */
	
	@override
	String toString()
	{
		return 'SERVER.JSON:'
				'\n\tAddr: $addr'
				'\n\tUsername: $username';
	}

	@override
	Map<String, dynamic> toJson()
	{
		return <String, dynamic>{
			JSON_SERVER_DOT_JSON_ADDR     : this.addr.toJson(),
			JSON_SERVER_DOT_JSON_USERNAME : this.username,
		};
	}
}
