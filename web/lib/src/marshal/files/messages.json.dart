import 'package:networkingfinal_client/src/marshal/json/message.dart';
import 'package:networkingfinal_client/src/marshal/marshal.dart';

/// A <messages.json>. Holds a list of [Message]s.
class MessagesDotJson implements Marshal<List<dynamic>>
{
	/*
	 * MEMBERS
	 */
	
	final List<Message> _contents;

	/*
	 * CONSTRUCTORS
	 */
	
	/// Create a [MessagesDotJson] from scratch.
	/// The elements of [contents] are automatically sorted by date sent.
	MessagesDotJson(List<Message> contents) :
		this._contents = contents..sort(
			(Message m1, Message m2) => m1.time.messageTimeFmt().asDateTime().compareTo(m2.time.messageTimeFmt().asDateTime())
		);

	/// Parse some [json] and create an instance of the [Message] class.
	MessagesDotJson.fromJson(List<dynamic> json) : this(
		json.cast<Map<String, dynamic>>().map<Message>(
			(Map<String, dynamic> msgJson) => new Message.fromJson(msgJson)
		).toList()
	);

	/*
	 * GETTERS
	 */
	
	/// The contents of a <messages.json> file.
	List<Message> get contents => this._contents;

	/*
	 * METHODS
	 */
	
	@override
	String toString()
	{
		return this.contents.fold<String>(
			'MESSAGES.JSON:',
			(String prev, Message m) => '$prev\n\t${m.toString()}'
		);
	}

	@override
	List<Map<String, String>> toJson()
	{
		return this.contents.map<Map<String, String>>(
			(Message m) => m.toJson()
		).toList();
	}
}
