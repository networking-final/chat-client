import 'dart:html';

/*
 * PANE
 */

/// A class containing all the methods needed to query
/// the [index.html](../../../web/index.html) elements.
abstract class Query
{
	static T _select<T extends Element>(final Element root, final String selector)
	{
		return ( (root == null) ? querySelector(selector) : root.querySelector(selector) ) as T;
	}

	static ElementList<T> _selectAll<T extends Element>(final Element root, final String selector)
	{
		return ( (root == null) ? querySelectorAll<T>(selector) : root.querySelectorAll<T>(selector) );
	}

	/// The root of the HTML [document](../web/index.html).
	static DivElement pane([Element root])
		=> Query._select<DivElement>(root, '#pane');

	/// Get the `#pane_chat` [DivElement].
	///
	/// * If [root] is specified, run the query from that [Element].
	static DivElement paneChat([Element root])
		=> Query._select<DivElement>(root, '#pane_chat');

	/// Get the `#pane_sidebar` [DivElement].
	///
	/// * If [root] is specified, run the query from that [Element].
	static DivElement paneSidebar([Element root])
		=> Query._select<DivElement>(root, '#pane_sidebar');

	/*
	 * PANE_CHAT
	 */

	/// Get the `#pane_chat_header` [DivElement].
	///
	/// * If [root] is specified, run the query from that [Element].
	static DivElement paneChatHeader([Element root])
		=> Query._select<DivElement>(root, '#pane_chat_header');


	/// Get the `#pane_chat_messages` [UListElement].
	///
	/// * If [root] is specified, run the query from that [Element].
	static UListElement paneChatMessageList([Element root])
		=> Query._select<UListElement>(root, '#pane_chat_message_list');

	/// Get the `#pane_chat_input` [InputElement].
	///
	/// * If [root] is specified, run the query from that [Element].
	static InputElement paneChatInput([Element root])
		=> Query._select<InputElement>(root, '#pane_chat_input');

	/// Get the `#pane_chat_input_button` [ButtonElement].
	///
	/// * If [root] is specified, run the query from that [Element].
	static ButtonElement paneChatInputButton([Element root])
		=> Query._select<ButtonElement>(root, '#pane_chat_input_button');

	/// Get the `#pane_chat_input_text` [DivElement].
	///
	/// * If [root] is specified, run the query from that [Element].
	static DivElement paneChatInputText([Element root])
		=> Query._select<DivElement>(root, '#pane_chat_input_text');

	/// Get the `#pane_chat_input_text_area` [TextAreaElement].
	///
	/// * If [root] is specified, run the query from that [Element].
	static TextAreaElement paneChatInputTextArea([Element root])
		=> Query._select<TextAreaElement>(root, '#pane_chat_input_text_area');

	/*
	 * PANE_SIDEBAR
	 */

	/// Get the `#pane_sidebar_settings` [DivElement].
	///
	/// * If [root] is specified, run the query from that [Element].
	static UListElement paneSidebarSettingList([Element root])
		=> Query._select<UListElement>(root, '#pane_sidebar_setting_list');

	/// Get every `.pane_sidebar_setting` [LIElement].
	///
	/// * If [root] _is_ specified, run the query from that [Element].
	/// * If [root] is _not_ specified, run the query from [Query.paneSidebarSettings].
	static ElementList<LIElement> paneSidebarSettings([Element root])
		=> Query._selectAll<LIElement>(
			( root ?? paneSidebarSettingList() ),
			'.pane_sidebar_setting'
		);

	/// Get the `.pane_sidebar_setting_entry` [DataElement] from [setting].
	static LabelElement paneSidebarSettingEntry(LIElement setting)
		=> Query._select<LabelElement>(
			setting, '.pane_sidebar_setting_entry'
		);

	/// Get the `.pane_sidebar_setting_dropdown` [DivElement] from [setting].
	static DivElement paneSidebarSettingDropdown(LIElement setting)
		=> Query._select<DivElement>(
			setting, '.pane_sidebar_setting_dropdown'
		);

	/// Get the `.pane_sidebar_setting_dropdown` [DivElement] from [dropdown].
	static ElementList<DivElement> paneSidebarSettingDropdownEntries(DivElement dropdown)
		=> Query._selectAll<DivElement>(
			dropdown, '.pane_sidebar_setting_dropdown_entry'
		);

	/// Get the `#pane_sidebar_room_list` [UListElement].
	///
	/// * If [root] is specified, run the query from that [Element].
	static UListElement paneSidebarRoomList([Element root])
		=> Query._select<UListElement>(root, '#pane_sidebar_room_list');

	/// Get every `.pane_sidebar_room` [LIElement].
	///
	/// * If [root] is specified, run the query from that [Element].
	/// * If [root] is _not_ specified, run the query from [Query.paneSidebarRoomList].
	static ElementList<LIElement> paneSidebarRooms([Element root])
		=> Query._selectAll<LIElement>(
			( root ?? paneSidebarRoomList() ),
			'.pane_sidebar_room'
		);

	/// Get the `.pane_sidebar_room_id` [DataElement] from [room].
	static DataElement paneSidebarRoomId(LIElement room)
		=> Query._select<DataElement>(room, '.pane_sidebar_room_id');

	/// Get the `.pane_sidebar_room_messages` [UListElement] from [room].
	static UListElement paneSidebarRoomMessageList(LIElement room)
		=> Query._select<UListElement>(room, '.pane_sidebar_room_message_list');
}
