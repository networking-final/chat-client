import 'dart:async';
import 'dart:html';

import 'package:networkingfinal_client/chat_client.dart';

/// 
abstract class PaneSidebarRoomList
{
	/*
     * MEMBERS
	 */

	static final List<StreamSubscription<MouseEvent>> _subscriptions = new List<StreamSubscription<MouseEvent>>();
	static       String                               _loadedRoomId;

	/*
	 * GETTERS
	 */

	/// The room that [Pane.chat.messages] are currently being loaded from.
	static String get loadedRoomId => _loadedRoomId;

	/*
	 * METHODS
	 */

	/// Listen to `.pane_sidebar_server_room` `onClick` events, and perform relevant actions.
	static void listenClick()
	{
		new Log('pane_sidebar_room_list_click()').write(
			'Adding event listeners for the room list.'
		);
		// Select all server room entries
		for ( final LIElement room in Query.paneSidebarRooms() )
		{
			_subscriptions.add(room.onClick.listen(
				(MouseEvent ev)
				{
					// Update the header
					PaneChatHeader.update(room);

					// Sync messages
					PaneChatMessageList.swap(room);

					// Update the ID to match the newly loaded room
					PaneSidebarRoomList._loadedRoomId = Query.paneSidebarRoomId(room).value;
				}
			));
		}
	}

	/// Clear the list of <room>s from the sidebar and unsubscribe from their listeners.
	static void resetListeners()
	{
		final Log log = new Log('pane_sidebar_room_list_reset()')..write('Clearing subscriptions.');

		// Unscubscribe all the listeners that had been created before.
		// They will need to be recreated before next use.
		while (PaneSidebarRoomList._subscriptions.isNotEmpty)
		{
			PaneSidebarRoomList._subscriptions.removeLast().cancel();
		}

		log.write('Cleared.', depth: 1);

		// Clear the list of rooms from the sidebar.
		Query.paneSidebarRoomList().children.clear();
	}
}
