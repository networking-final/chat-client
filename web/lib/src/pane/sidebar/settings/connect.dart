import 'dart:html';

import 'package:networkingfinal_client/chat_client.dart';
import 'package:networkingfinal_client/src/net/ws/connection_manager.dart';

/// Class defining operations on the connect `<pane_sidebar_setting>`.
abstract class PaneSidebarSettingConnect
{
	/*
	 * MEMBERS
	 */

	/// The "Connect" setting.
	static final LIElement _settingElement = Query.paneSidebarSettings().firstWhere(
		(LIElement el) => Query.paneSidebarSettingEntry(el).innerText == 'Connect'
	);

	/// The active server connection.
	static ConnectionManager _connMan;

	/// The index at which the 'server_ip' field occurs in the server connect form.
	static const int _INDEX_SERVER_IP   = 1;
	/// The index at which the 'server_name' field occurs in the server connect form.
	static const int _INDEX_SERVER_NAME = 0;
	/// The index at which the 'server_port' field occurs in the server connect form.
	static const int _INDEX_SERVER_PORT = 2;
	/// The index at which the 'submit' button occurs in the server connect form.
	static const int _INDEX_SUBMIT      = 4;
	/// The index at which the 'username' field occurs in the server connect form.
	static const int _INDEX_USERNAME    = 3;

	static final List<InputElement> _input = Query.paneSidebarSettingDropdownEntries(
		// Get the '.pane_sidebar_setting_dropdown_entry's from the [_connectSetting].
		Query.paneSidebarSettingDropdown(_settingElement)
	// Transform the list of Entries into the input elements from the form.
	).fold<List<InputElement>>(
		// Start with a new list of `InputElement`s.
		new List<InputElement>(),
		// Create a function that determines which items should be thrown out.
		(List<InputElement> inputElements, DivElement e)
		{
			// We only want input elements.
			if (e.children.first is InputElement)
			{ return inputElements..add(e.children.first as InputElement); }
			// If it isn't an input element, ignore it.
			else
			{ return inputElements; }
		}
	);

	/*
	 * METHODS
	 */


	/// Function to run when the submit button is clicked.
	static void _handleConnectClick()
	{
		// Get connected to the server (i.e. start the chat application.)
		new Log('_connectClick()').write('Starting server connection.');

		// Close the previous connection.
		PaneSidebarSettingConnect._connMan?.close();

		// Update the `localStorage`.
		PaneSidebarSettingConnect._updateLocalStorageDir();

		// Assign new connection.
		PaneSidebarSettingConnect._connMan = new ConnectionManager(
			PaneSidebarSettingConnect._input[_INDEX_SERVER_NAME].value
		)..listen();
	}

	/// Update the theme of the webpage based on a button press.
	static void listenConnectClick()
	{
		// Fill the room select menu.
		PaneSidebarSettingConnect._populateServerSelector();
		// THEN and ONLY THEN can we begin to respond to events in the connect menu.
		PaneSidebarSettingConnect._listenServerSelectorClick(); /* 1st */
		PaneSidebarSettingConnect._listenConnectClick();        /* 2nd */
	}

	/// Clicking the connect button.
	static void _listenConnectClick()
	{
		// Listen for click events on the room select element.
		final Log log = new Log('_onButtonClick()')
		// Listen for click events on the submit button.
		..write('Adding `onClick` listener.');
		PaneSidebarSettingConnect._input[_INDEX_SUBMIT].onClick.listen(
			(MouseEvent ev)
			{
				// Stop page from refreshing on submit.
				ev.preventDefault();

				// Iterate over `.pane_sidebar_setting_dropdown`.
				log.write('Looking for the `FormElement`.');
				for (final Element e in Query.paneSidebarSettingDropdown(_settingElement).children)
				{
					// When the input form child is found...
					if (e is FormElement)
					{
						log.write('Found the form element.');
						// ...perform an operation provided everything is filled in.
						if (e.reportValidity())
						{
							// Make sure the data is valid as well.
							log.write('Testing form data...');
							if (PaneSidebarSettingConnect._testFormData())
							{
								log.write('Valid. Handling click.');
								PaneSidebarSettingConnect._handleConnectClick();
							}
							else
							{ log.write('Invalid. Dropping click.'); }
						}
						else
						{ log.write('It is invalid.'); }

						// No need to keep looking for the Form.
						break;
					}
				}
			}
		);
	}


	static void _listenServerSelectorClick()
	{
		final Log log = new Log('_serverSelectorOnClick()')..write(
			'Adding listener for the server `SelectElement`'
		);

		// Get the server <select>ion element.
		final SelectElement serverSelector = Query.paneSidebarSettingDropdown(
			PaneSidebarSettingConnect._settingElement
		).querySelector('select') as SelectElement;

		// Add a listener for whenever there is a change on what option is selected.
		serverSelector.onChange.listen(
			(Event ev)
			{
				// Get the selected option.
				log.write('Find the selected list option.');
				final OptionElement selected = serverSelector.options[serverSelector.selectedIndex];

				// Update the fields.
				log.write('Updating form values to match cached room values.');
				PaneSidebarSettingConnect._input[_INDEX_SERVER_IP  ].value = LocalStorageDir.get(selected.value).serverFile.addr.ip;
				PaneSidebarSettingConnect._input[_INDEX_SERVER_NAME].value = selected.value;
				PaneSidebarSettingConnect._input[_INDEX_SERVER_PORT].value = LocalStorageDir.get(selected.value).serverFile.addr.port.toString();
				PaneSidebarSettingConnect._input[_INDEX_USERNAME   ].value = LocalStorageDir.get(selected.value).serverFile.username;

				// Reset the selecteion (to avoid confusion if the fields are edited later).
				serverSelector.selectedIndex = 0;
			}
		);
	}

	/// Fill the connection presets with the names of rooms.
	static void _populateServerSelector()
	{

		// Grab the `SelectElement`
		final Log log = new Log('_fillServerSelector()')..write('Grabbing <select> element.');
		final SelectElement serverSelector = Query.paneSidebarSettingDropdown(_settingElement).children.firstWhere(
			(Element e) => e is SelectElement
		) as SelectElement;

		log.write('Filling in the dropdown options.');
		for (final String serverName in LocalStorageDir.keys)
		{
			log.write('Adding "$serverName"', depth: 1);
			// Add Option elements.
			serverSelector.children.add(
				new OptionElement()
					..innerText = serverName
					..value     = serverName
			);
		}
	}

	/// Determine if the dataset submitted is valid.
	static bool _testFormData()
	{
		final Log log = new Log('_testFormData()');
		try
		{
			// Begin testing [ip].
			log.write('Testing [ip]...');
			final String ip = PaneSidebarSettingConnect._input[_INDEX_SERVER_IP].value.trim();
			// Try to parse as IPv4.
			try
			{
				log.write('...as IPv4.', depth: 1);
				Uri.parseIPv4Address(ip);
			}
			// If that fails, try to parse as IPv6.
			on FormatException
			{
				log.write('...as IPv6.', depth: 1);

				// If THIS fails, mark the field as invalid and rethrow.
				try
				{
					Uri.parseIPv6Address(ip);
				}
				on FormatException
				{
					PaneSidebarSettingConnect._input[_INDEX_SERVER_IP].setCustomValidity('Please enter a valid IPv4 or IPv6 address.');
					rethrow;
				}
			}

			// Try to turn the port into an int.
			log.write('Testing port...');

			try
			{
				int.parse(PaneSidebarSettingConnect._input[_INDEX_SERVER_PORT].value.trim());
			}
			on FormatException
			{
				PaneSidebarSettingConnect._input[_INDEX_SERVER_PORT].setCustomValidity('Port contains non-number characters.');
				rethrow;
			}


			// Check for to assure only legal characters are in the names.
			// The regex tests for any character that is NOT a number, letter (lower|upper case), "-", "_", or "." and throws an error if one is found.
			log.write('Testing names...');
			for
			(final InputElement toTest in <InputElement>[
				PaneSidebarSettingConnect._input[_INDEX_SERVER_NAME], PaneSidebarSettingConnect._input[_INDEX_USERNAME]
			])
			{
				toTest.value = toTest.value.trim();
				log.write('...${toTest.placeholder}', depth: 1);
				if (toTest.value.isEmpty || toTest.value.contains(new RegExp(r'[^0-9a-zA-Z\-\_\.]')))
				{
					toTest.setCustomValidity('${toTest.placeholder} contains invalid characters.');
					throw FormatException('Illegal character.');
				}
			}
		}
		on Error catch (e)
		{
			log.write(e.toString(), depth: 1);
			return false;
		}
		on Exception catch (e)
		{
			log.write(e.toString(), depth: 1);
			return false;
		}

		// Assuming no errors or exceptions were thrown, the dataset must be good.
		return true;
	}

	/// Calculate what the [LocalStorageDir] should be.
	static void _updateLocalStorageDir()
	{
		// Read localStorage.
		final Log log = new Log('_getLocalStorageDir()')..write('Reading local storage.');

		// If the server is pre-existing update the information that's already there.
		if (LocalStorageDir.containsKey( PaneSidebarSettingConnect._input[_INDEX_SERVER_NAME].value ))
		{
			log.write('The server was previously known. Updating information.');
			LocalStorageDir.get(PaneSidebarSettingConnect._input[_INDEX_SERVER_NAME].value).serverFile
				..addr.ip   = PaneSidebarSettingConnect._input[_INDEX_SERVER_IP].value
				..addr.port = int.parse(PaneSidebarSettingConnect._input[_INDEX_SERVER_PORT].value)
				..username  = PaneSidebarSettingConnect._input[_INDEX_USERNAME].value;
		}
		// If the server is new, create the entry.
		else
		{
			log.write('The server is unknown. Creating new `ServerDir`.');
			LocalStorageDir.put(PaneSidebarSettingConnect._input[_INDEX_SERVER_NAME].value,
				new ServerDir(
					new Map<String, RoomDir>(),
					new ServerDotJson(
						new ServerAddr(
							PaneSidebarSettingConnect._input[_INDEX_SERVER_IP].value,
							int.parse( PaneSidebarSettingConnect._input[_INDEX_SERVER_PORT].value )
						),
						PaneSidebarSettingConnect._input[_INDEX_USERNAME].value
					)
				)
			);
		}
	}

}
