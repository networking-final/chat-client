import 'dart:html';

import 'package:networkingfinal_client/chat_client.dart';

/// Class defining operations on the theme `<pane_sidebar_setting>`.
abstract class PaneSidebarSettingTheme
{
	static const String _PREFIX = '#selector_theme_';
	static const String _DARK   = '${_PREFIX}dark';
	static const String _LIGHT  = '${_PREFIX}light';

	/// Update the theme of the webpage based on a button press.
	static void listenSelector()
	{
		final Log log = new Log('pane_sidebar_settings_theme()')..write('Adding event listener for switching theme to DARK.');
		querySelector(_DARK).onClick.listen(
			(MouseEvent e)
			{
				// Load dark CSS
				querySelector('#theme_stylesheet').attributes['href'] = './style/dist/index_dark.css';
				// Toggle option in menu
				querySelector(_DARK).children.whereType<InputElement>().first.checked = true;
				querySelector(_LIGHT).children.whereType<InputElement>().first.checked = false;
				log.write('Changing theme to DARK.');
			}
		);

		log.write('Adding event listener for switching theme to LIGHT.');
		querySelector(_LIGHT).onClick.listen(
			(MouseEvent e)
			{
				// Load light CSS
				querySelector('#theme_stylesheet').attributes['href'] = './style/dist/index.css';
				// Toggle option in menu
				querySelector(_DARK).children.whereType<InputElement>().first.checked = false;
				querySelector(_LIGHT).children.whereType<InputElement>().first.checked = true;
				window.console.log('Changing theme to LIGHT.');
			}
		);
	}
}
