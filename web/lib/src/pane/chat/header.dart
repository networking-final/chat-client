import 'dart:html';

import 'package:networkingfinal_client/chat_client.dart';

/// Class representing operations on `<pane_chat_header>`.
abstract class PaneChatHeader
{
	/// Change the text of this header.
	static void update( LIElement room )
	{
		final Log log = new Log('pane_chat_header_update()');
		final String newHeader = room.children.first.innerText.replaceAll('_', ' ');
		log.write(
			'Changing `#pane_chat_header.text` to "$newHeader."',
			newLine: true
		);
		Query.paneChatHeader().children.first.innerText = newHeader;
	}
}
