import 'dart:async';
import 'dart:html';

import 'package:networkingfinal_client/chat_client.dart';
import 'package:networkingfinal_client/src/net/ws/connection_manager.dart';

/// Class representing operations on `<pane_chat_input>`.
abstract class PaneChatInput
{
	/// Define a click event so that the contents of `<pane_chat_input_text>` are formatted into a [MessageDequeued] and sent to [connMan].
	static void listenButtonClick(final ConnectionManager connMan)
	{
		final Log log = new Log('pane_chat_input_button_click')..write('Subscribing to input button.');

		// Register listener for send button.
		final StreamSubscription<MouseEvent> inputButtonListener = Query.paneChatInputButton().onClick.listen(
			(MouseEvent ev) => PaneChatInput._handleInputEvent(connMan, log)
		);

		// Register listener for enter key.
		final StreamSubscription<KeyboardEvent> submitListener = Query.paneChatInputButton().onKeyUp.listen(
			(KeyboardEvent ev)
			{
				if (ev.keyCode == 13 && !ev.shiftKey)
				{ PaneChatInput._handleInputEvent(connMan, log); }
			}
		);

		// Make sure to cancel this listener when the server closes.
		connMan.onClose?.listen(
			(CloseEvent ev)
			{
				inputButtonListener.cancel();
				submitListener.cancel();
			}
		);
	}

	static void _handleInputEvent(final ConnectionManager connMan, final Log log)
	{
		log.write('Grabbing `pane_chat_input_text`.');
		final String messageBody = Query.paneChatInputTextArea().value.trim();

		// Don't send a message if there's nothing to send.
		if (messageBody.isNotEmpty) // †
		{
			if (PaneSidebarRoomList.loadedRoomId != null && PaneSidebarRoomList.loadedRoomId.isNotEmpty)
			{
				log.write('Message was not empty.');

				// Get the current time.
				final DateTime now = new DateTime.now();

				// Grab the room id.
				final String roomId = PaneSidebarRoomList.loadedRoomId;

				final MessageDequeued msg = new MessageDequeued(
					new Message(
						connMan.serverDir.serverFile.username,
						messageBody,
						'${now.year}-${now.month}-${now.day}_${now.hour}-${now.minute}-${now.second}'
					),
					roomId
				);

				// Send to the client appropriate JSON.
				// NOTE: see the documentation for the <message> operation.
				log.write('Sending [msg] to server.');
				connMan.sendJson(new SocketOpResponse(
					msg, new SocketOp.asPost(SocketOp.TYPE_MSG)
				));

				// Add the message to `localStorage` and the DOM.
				log.write('Adding the message to relevant places.');
				PaneChatMessageList.add(connMan, msg);

				// Reset the text in the text area. (All modern chat clients do this...)
				Query.paneChatInputTextArea().value = '';
			} // ‡
			else
			{
				window.alert('Select a room before sending a message.');
			}
		}
		else
		{
			window.alert('Cannot send empty message.');
		}
	}
}
