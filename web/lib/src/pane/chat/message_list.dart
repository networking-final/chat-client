import 'dart:html';

import 'package:meta/meta.dart';

import 'package:networkingfinal_client/chat_client.dart';
import 'package:networkingfinal_client/src/net/ws/connection_manager.dart';

/// Class representing operations on `<pane_chat_message_list>`.
abstract class PaneChatMessageList
{
	/*
	 * METHODS
	 */

	/// Add some [msg] to `localStorage` _and_ the DOM.
	static void _add(final ConnectionManager connMan, final MessageDequeued msg)
	{
		// Add the Message to the list of messages for the room.
		final Log log = new Log('_add()')..write('Adding message to ${msg.room}');
		connMan.serverDir.roomIdMap[msg.room].messagesDotJson.contents.add(msg.message);

		// Variable to hold the target for the new message.
		UListElement updateTarget;

		// Check whether or not the current message list is for the room the new message is in.
		log.write('Assessing update target.');
		if (PaneSidebarRoomList.loadedRoomId != null && PaneSidebarRoomList.loadedRoomId.isNotEmpty && PaneSidebarRoomList.loadedRoomId == msg.room)
		{
			log.write('Target is `<pane_chat_message_list>`', depth: 1);
			updateTarget = Query.paneChatMessageList();
		}
		else
		{
			log.write('Target is `<pane_sidebar_room_message_list>`', depth: 1);
			updateTarget = Query.paneSidebarRoomMessageList(
				Query.paneSidebarRooms().firstWhere((LIElement room) => Query.paneSidebarRoomId(room).value == msg.room)
			);
		}

		// Create HTML for the message.
		if (updateTarget.children.isNotEmpty)
		{
			log.write('There were messages in the room. Attempting to reduce...');
			try
			{
				updateTarget.children.last = Message.reduceHtml(
					updateTarget.children.last as LIElement, msg.message
				);
				log.write('Reduced the messages. Returning successfuly.', depth: 1);
				return;
			}
			on ArgumentError
			{
				log.write('Failed to reduce the messages. Continuing...', depth: 1);
			}
		}

		log.write('Adding new message node to DOM.');
		updateTarget.children.add( msg.message.toHtml(connMan.serverDir.serverFile.username) );
	}

	/// Add some [msg] to [connMan]'s [LocalStorageDir] _and_ the DOM, then update `localStorage`.
	/// Different from [PaneChatMessageList.addAll()] in that it writes to `localStorage` every single time.
	static void add(final ConnectionManager connMan, final MessageDequeued msg)
	{
		PaneChatMessageList._add(connMan, msg);

		PaneChatMessageList._afterAdd();
	}

	/// Add [msgs] to [connMan]'s [LocalStorageDir] _and_ the DOM, then update `localStorage`.
	/// Different from [PaneChatMessageList.add()] in that it waits to write to `localStorage` until _after_ all messages are synced with the DOM.
	static void addAll(final ConnectionManager connMan, final List<MessageDequeued> msgs)
	{
		for (final MessageDequeued msg in msgs)
		{ PaneChatMessageList._add(connMan, msg); }

		PaneChatMessageList._afterAdd();
	}

	/// Common operations performed after [PaneChatMessageList._add()].
	static void _afterAdd()
	{
		new Log('_afterAdd()').write('Writing updated [LocalStorageDir] to `localStorage`.');

		// Sync the local storage with the in-memory content.
		LocalStorageDir.shelve();
	}

	/// Update the messages in the `<pane_chat_messages>` window.
	static void swap( LIElement room )
	{
		final Log log = new Log('pane_chat_messages_update()');

		void childSync({@required UListElement syncFrom, @required UListElement syncTo})
		{
			log.write('Syncing child elements.', depth: 1);
			syncTo.children..clear()..addAll(syncFrom.children);
			log.write('Done.', depth: 2);
		}

		// Archive the messages from the old room, IF there was an old room.
		log.write('Archiving messages.');
		if ( PaneSidebarRoomList.loadedRoomId == null )
		{
			log.write('There was no old room.', depth: 1);
		}
		else
		{
			log.write('Accessing the old room (#${PaneSidebarRoomList.loadedRoomId}).', depth: 1);

			childSync(
				syncFrom: Query.paneChatMessageList(),
				syncTo:   Query.paneSidebarRoomMessageList( /* Access messages */
					// Find the first room that matches the ID
					Query.paneSidebarRooms().firstWhere(
						(LIElement room)
							=> PaneSidebarRoomList.loadedRoomId == Query.paneSidebarRoomId(room).value
					)
				)
			);
		}

		// Load the new chatroom's messages
		log.write('Loading messages from room #${Query.paneSidebarRoomId(room).value}.');
		childSync(
			syncFrom: Query.paneSidebarRoomMessageList(room),
			syncTo: Query.paneChatMessageList()
		);
	}
}
