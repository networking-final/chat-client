import 'package:networkingfinal_client/chat_client.dart';
import 'package:networkingfinal_client/src/net/ws/connection_manager.dart';

/// Class containing handler methods for [SocketOp]s and [SocketOpResponse]s.
abstract class Handler
{
	static final List<MessageDequeued> _messageBuffer = new List<MessageDequeued>();

	/// Determine what to do with a given [operation].
	static void delegateOperation(final ConnectionManager connMan, final SocketOp operation)
	{//†
		final Log log = new Log('Handler.delegateOperation()');
		switch (operation.opType)
		{
			case SocketOp.OP_TYPE_OK: // †
				switch (operation.contentType)
				{
					case SocketOp.TYPE_CLIENT_SESSION: // †
						log.write('Server accepted the <client_session>');
						// Define a listener for `<pane_chat_input_button>` so that we can now send messages.
						PaneChatInput.listenButtonClick(connMan);
						// ‡
						break;

					case SocketOp.TYPE_FETCH: // †
						log.write('Server says there are messages for us in its <queue>.');
						connMan.sendJson(new SocketOp.asGet(SocketOp.TYPE_QUEUE));
						// ‡
						break;

					case SocketOp.TYPE_QUEUE: // †
						log..write('Server says there are no more messages for us in its <queue>.')
							..write('Flushing [_messageBuffer].')..write('$_messageBuffer', depth:1);
						// Write all of the messages in the buffer to the DOM & `localStorage`.
						PaneChatMessageList.addAll(connMan, Handler._messageBuffer);
						// Clear the buffer.
						Handler._messageBuffer.clear();
						// ‡
						break;

					default: // No matching <content_type>.
						throw new ArgumentError('The <content_type> is unrecognized');
						break;
				}
				// ‡
				break;

			// No matching <op_type>.
			default:
				log.write(operation.opType);
				throw new ArgumentError('The <op_type> is unrecognized for an operation.');
				break;
		}
	}//‡

	/// Determine what to do with a given [response].
	static void delegateResponse(final ConnectionManager connMan, final SocketOpResponse response)
	{//†
		final Log log = new Log('Handler.delegateResponse()');

		switch (response.operation.opType)
		{
			case SocketOp.OP_TYPE_POST: // †
				switch (response.operation.contentType)
				{
					case SocketOp.TYPE_CLIENT_SESSION: // †
						log.write('New <client_session>.');
						// Set the content.
						connMan.serverDir.session = response.contentJson as String;
						LocalStorageDir.shelve();
						// ‡
						break;

					case SocketOp.TYPE_QUEUE: // †
						log.write('New <message_dequeued>. \n${response.contentJson}');
						// Add the message to the message buffer.
						Handler._messageBuffer.add(new MessageDequeued.fromJson(
							response.contentJson as Map<String, dynamic>
						));
						// ‡
						break;

					case SocketOp.TYPE_ROOMS: // †
						Handler._handleResponseRooms(connMan, response);
						// ‡
						break;

					// Unrecognized <content_type>.
					default:
						throw new ArgumentError('The <content_type> is unrecognized');
						break;
				}
				// ‡
				break;

			// No matching <op_type>.
			default:
				throw new ArgumentError('The <op_type> is unrecognized for a response.');
		}
	}//‡

	/// Handle a [response] using [connMan] if [response.operation.contentType] is [SocketOp.TYPE_ROOMS].
	static void _handleResponseRooms(final ConnectionManager connMan, final SocketOpResponse response)
	{ // †
		final Log log = new Log('_handleResponseRooms()')..write('New <room_info> list.');

		// Set the content.
		response.content = (response.contentJson as List<dynamic>).map<RoomInfo>(
			(dynamic d) => new RoomInfo.fromJson(d as Map<String, dynamic>)
		).toList(growable:false);
		log.write('Unmarshalled <room_info> list.');

		// Store the rooms that the server gave us.
		for ( final RoomInfo room in (response.content as List<RoomInfo>) )
		{ // †
			// If the room is new to the client.
			log.write('Checking if room "${room.name}" is previously known...');
			if (!connMan.serverDir.roomIdMap.containsKey(room.id))
			{
				// Create a new room.
				log.write('The room is new. Creating.', depth: 1);
				connMan.serverDir.roomIdMap[room.id] = new RoomDir(
					new MessagesDotJson(new List<Message>()), new RoomDotJson(room.name)
				);
			}
			// Otherwise, update the values of the existing room. This way, we don't lose our messages by replacing our old room.
			else
			{
				log.write('Room already existed. Updating information.', depth: 1);
				connMan.serverDir.roomIdMap[room.id].roomDotJson.name = room.name;
			}
		} // ‡

		// Write the changes to localStorage.
		log.write('Stashing changes to local storage.');
		LocalStorageDir.shelve();

		// Clear the previously loaded rooms from DOM.
		// NOTE: This should not clear them from `localStorage`, or affect the savestate of the program. Keeping the `localStorage` synced should occur elsewhere (like literally two lines above, for example), not here.
		log.write('Clearing sidebar.');
		PaneSidebarRoomList.resetListeners();

		for
		(final String id in connMan.serverDir.roomIdMap.keys.where(
			// †
			// We only want to list rooms that the server has told us to list, EVEN IF we know about more.
			// If we show rooms we have no visibility for, we will receive an error 4004.
			(String key) => (response.content as List<RoomInfo>).any((RoomInfo i) => i.name == connMan.serverDir[key].roomDotJson.name)
		).toList()..sort(
			// Sort the [RoomDir]s by `<room_name>`.
			// NOTE: This might sort the rooms the wrong way.
			(String k1, String k2) => connMan.serverDir[k1].roomDotJson.name.compareTo(connMan.serverDir[k2].roomDotJson.name)
			// ‡
		))
		{
			connMan.serverDir[id].updatePaneSidebar(
				roomId: id,
				username: connMan.serverDir.serverFile.username
			);
		}

		log.write('Successfully repopulated sidebar. Resubscribing to new rooms...');
		// Re-add the listeners.
		PaneSidebarRoomList.listenClick();
	} // ‡
}

