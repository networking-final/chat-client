import 'dart:async';
import 'dart:html';

import 'package:networkingfinal_client/chat_client.dart';
import 'package:networkingfinal_client/src/net/ws/handler.dart';
import 'package:networkingfinal_client/src/util/log/log.dart';

/// Group of functions to interact with a [WebSocket] server.
class ConnectionManager
{
	/*
	 * MEMBERS
	 */

	final String    _serverName;
	      WebSocket _serverSocket;

	/*
	 * CONSTRUCTORS
	 */

	/// Create a new [ConnectionManager] for a specific [ServerDir].
	ConnectionManager(this._serverName);

	/*
	 * GETTERS
	 */

	/// Grab the [_serverSocket]'s `onClose` event. May return `null`.
	Stream<CloseEvent> get onClose => this._serverSocket?.onClose;

	/// Get the [ServerDir] corresponding to the name of the server that is actively connected.
	ServerDir get serverDir => LocalStorageDir.get(this._serverName);

	/*
     * METHODS
	 */

	/// Close the active connection that this [ConnectionManager] is tracking.
	void close() => this._serverSocket?.close();

	void _listenDoneEvents()
	{//†
		final Log log = new Log('_handleCloseEvents');

		this._serverSocket.onClose.listen(
			(CloseEvent ev)
			{
				log..write('Server has closed.')
					..write('Status Code: ${ev.code}')
					..write('Reason: ${ev.reason}');

				switch (ev.code)
				{ // †
					// The server indicates that our username contains invalid characters, and we do not have a session key.
					case 4000:
						window.alert('Username contains invalid characters.');
						break;

					// The server indicates that our username is taken and we do not have a session key.
					case 4001:
						window.alert('Username taken');
						break;

					// The server indicates that the username is unknown but we tried to login with a session key anyway.
					case 4002:
						window.alert('Server does not recognize the session.');
						break;

					// The server indicates that our login (username:session combination) is invalid.
					case 4003:
						window.alert('Login combination invalid. Did you change your username?');
						break;

					// The server indicates that we have sent a message to a room that does not exist.
					case 4004:
						window.alert("You sent a message to a room that doesn't exist on the server.");
						break;

					// The server indicates that we have sent a message to a room that we were not supposed to know about.
					case 4005:
						window.alert('You sent a message to a room that the server did not give you access to.');
						break;

					// The server indicates that it is full.
					case 4006:
						window.alert("The server's message cache is full for this room.");
						break;

					// Unaccounted-for error.
					default:
						window.alert('Something went wrong. Please see the log.');
						break;
				} // ‡
			}
		);
	}//‡

	void _listenErrorEvents()
	{
		final Log log = new Log('_handleErrorEvents()');

		this._serverSocket.onError.listen(
			(Event ev)
			{
				log.write(ev.toString());
			}
		);
	}

	/// Handle an incoming message.
	/// * Called by [listen] through [\_serverSocket]'s `onClick` event.
	void _listenMessageEvents()
	{//†
		// Define the logger for this method.
		final Log log = new Log('_handleMessage()');

		// Define what should happen when `onMessage`.
		this._serverSocket.onMessage.listen(
			(MessageEvent ev)
			{
				log.write('Server sent a message.\n${ev.data}\n');

				final Marshal<Map<String, dynamic>> msg = SocketOpResponse.analyze(
					Marshal.convert<Map<String, dynamic>>(ev.data as String)
				);

				// The message is an operation.
				if (msg is SocketOp)
				{
					log.write('Received operation from server.');
					Handler.delegateOperation(this, msg);
				}
				// The message is a response.
				else if (msg is SocketOpResponse)
				{
					log.write('Received response from server.');
					Handler.delegateResponse(this, msg);
				}
				// Somehow an unrecognized [Marshal] has been accepted by [SocketOpResponse.analyze()].
				else
				{ throw new StateError('`analyze()` somehow returned a non-regular value.'); }
			}
		);
	}//‡

	/// Start a connection to the server using a [WebSocket].
	/// Gathers information from the [serverDir].
	void listen()
	{//†
		final String serverAddr = 'ws://${this.serverDir.serverFile.addr.ip}:'
			'${this.serverDir.serverFile.addr.port}/chat-server';
		// Start the connection.
		final Log log = new Log('start()')..write('Connecting to $serverAddr');
		this._serverSocket = new WebSocket(serverAddr);
		// Assuming there was no error, the connection was successful.
		log.write('Successfuly connected.', depth: 1);

		// Begin listening [Event]s.
		this._listenDoneEvents();
		this._listenErrorEvents();
		this._listenMessageEvents();

		// Send initial message announcing the presence of our client.
		// Without this, the server would never send anything back to us.
		this._serverSocket.onOpen.listen(
			(Event ev)
			{
				log.write('Server is ready. Sending initial message.');
				this.sendJson(
					new SocketOpResponse(
						new Client(serverDir.serverFile.username, serverDir.session),
						new SocketOp.asPost(SocketOp.TYPE_CLIENT),
					)
				);
			}
		);
	}//‡

	/// Send some [msg] through [_serverSocket].
	Future<void> sendJson(final Marshal<Map<String, dynamic>> msg) async
	{//†
		final Log log = new Log('sendJson()')..write(
			'Writing message to server.'
		);

		if (this._serverSocket == null)
		{
			log.write('There is no active connection.');
			throw new StateError('The Connection Manager is not listening to a server, so it cannot send anything.');
		}
		else
		{
			// Loop until message is sent.
			// NOTE: it would be possible to add a timeout function here, but I believe Dart handles this automatically.
			while (true)
			{
				// If the server is ready to receive.
				if (this._serverSocket.readyState == 1)
				{
					// Send the message.
					log.write('Server accepted the message.', depth: 1);
					this._serverSocket.sendString(Marshal.revert(msg.toJson()));
					break;
				}
				// If the server is not ready to receive.
				else
				{
					// Delay the action in an asyncronous manner.
					// This is different from `sleep()` which stops all execution.
					log.write('Server was not ready for message. Waiting 25ms...', depth: 1);
					await new Future<dynamic>.delayed(new Duration(milliseconds: 25));
				}
			}
		}//‡
	}
}
