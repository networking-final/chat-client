import 'dart:html';

/// Clears the [window]'s console object.
void clearConsole()
{
	window.console..clear('')..log('');
}
