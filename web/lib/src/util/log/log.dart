import 'dart:html';

/// A class that interfaces with `window.console.log()` to consistently format strings.
class Log
{
	final String _logPrefix;

	/// Create a new [Log] that prefixes all logs with [_logPrefix].
	Log(this._logPrefix);

	/// Log [text] to the console with consistent formatting according to `this` class' construction.
	/// * If [depth] is specified, indent [text] [depth] many times.
	void write( String text, {final bool newLine = false, final int depth = 0} )
	{
		String logText = '– $_logPrefix: ';
		if ( depth > 0 )
		{
			for ( int i = 0; i < depth; ++i )
			{
				logText = '$logText    ';
			}
		}

		if (newLine)
		{
			logText = '\n$logText';
		}
		window.console.log('$logText$text');
	}
}
