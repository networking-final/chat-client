#!/usr/bin/env bash
## Vars
dir="./web/style"

src="src"
dist="dist"

## Make sure that the target exists.
mkdir "$dir/$dist"

## SCSS → CSS
files=(index index_dark)
for f in ${files[*]} ; do
	sass "$dir/$src/$f.scss" "$dir/$dist/$f.css"
done
