#!/usr/bin/env bash
## Vars
dir="./web/script"

src="src"
dist="dist"

## Make sure that the directory exists
mkdir "$dir/$dist"

## Grab dependencies for project.
pub get

## Dart → JS
files=(main)
for f in $files ; do
	dart2js "$dir/$src/$f.dart" -o "$dir/$dist/$f.js"
done
